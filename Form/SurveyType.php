<?php

namespace Ibw\SurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SurveyType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('displayType', 'choice', array(
                'choices'   => \Ibw\SurveyBundle\Entity\Survey::$surveyTypes
            ))
            ->add('title', null, array(
                'required'  => false
            ))
            ->add('messageFile')
            ->add('displayTime', 'date', array(
                'required'  => false,
            ))
            ->add('canBeDelayed', null, array(
                'required'  => false
            ))
            ->add('isActive', null, array(
                'required'  => false
            ))
            ->add('criteria')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ibw\SurveyBundle\Entity\Survey'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ibw_surveybundle_survey';
    }
}
