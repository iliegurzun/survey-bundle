var Survey = {
    surveyId: null,
    route: null,
    logRoute: null,
    userId: null,
    init: function(params)
    {
        this.surveyId = params.surveyId;
        this.route = params.route;
        this.logRoute = params.logRoute;
        this.userId = params.userId;
        var currentSurvey = this;
        $.ajax({
            url: this.route,
            type: 'get'
        }).success(function(data)
        {
            if(getSurveyCookie('survey-'+currentSurvey.userId+'-'+currentSurvey.surveyId) == '1')
            {
                return;
            }
           if(data.success == true)
           {
               if(data.templateType == 1)
               {
                    $('body').append(data.content);
                    $('#ibw-survey').modal('show');
                    
                    $('#ibw-survey').on('shown.bs.modal', function() 
                    {
                        if(typeof _gaq !=='undefined')
                        {
                            _gaq.push(['_trackEvent', 'Message', 'Display', 'Satisfaction Survey', 1, true]);
                        }
                    });
                    
                    $('#ibw-survey').on('hidden.bs.modal', function () {
                        setSurveyCookie('survey-'+currentSurvey.userId+'-'+currentSurvey.surveyId, 1, 1);
                        if(typeof _gaq !=='undefined')
                        {
                            _gaq.push(['_trackEvent', 'Message', 'Close', 'Satisfaction Survey']);
                        }
                    });
               }
               else if(data.templateType == 2)
               {
                    $('#append-flash-survey').html(data.content);
                    $('#ibw-survey').find('a#survey-remind-later').on('click', function(e)
                    {
                        setSurveyCookie('survey-'+currentSurvey.userId+'-'+currentSurvey.surveyId, 1, 1);
                        $('#ibw-survey').fadeOut();
                    });
                    $('#ibw-survey .close-flash').on('click', function () {
                        if(typeof _gaq !=='undefined')
                        {
                            _gaq.push(['_trackEvent', 'Message', 'Close', 'Satisfaction Survey']);
                        }
                        $('#ibw-survey').fadeOut();
                    });
               }
               
                $('#survey-remind-later').on('click', function()
                {
                    if(typeof _gaq !=='undefined')
                    {
                        _gaq.push(['_trackEvent', 'Message', 'Remind', 'Satisfaction Survey']);
                    }
                });
                
                $('#ibw-survey').find('a:not(.close-flash):not(#survey-remind-later)').on('click', function(e)
                {
                   if(typeof _gaq !=='undefined')
                   {
                       _gaq.push(['_trackEvent', 'Message', 'Click', 'Satisfaction Survey']);
                   }
                   logUserAction(currentSurvey);
                });
           }
        }).error(function(data)
        {
            alert(data.responseJSON.error);
        });
    },
    
}


var logUserAction = function(survey)
{
    $.ajax({
        url: survey.logRoute,
        type: 'POST'
    }).done(function(data)
    {
        
    });
}

function setSurveyCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
} 

function getSurveyCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
} 