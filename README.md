IntelligentBee Survey Bundle
========================

1) Add bundle into composer.json
----------------------------------
```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://iliegurzun@bitbucket.org/iliegurzun/survey-bundle.git"
    }
],
```
```
"require": {
    "iliegurzun/survey-bundle": "dev-master"
}
```
2) Install composer
----------------------------------
```
php composer.phar install
```

3) Include the Bundle into AppKernel
----------------------------------

```
new Ibw\SurveyBundle\IbwSurveyBundle()
```


4) Include routes
----------------------------------
```
ibw_survey:
    resource: "@IbwSurveyBundle/Resources/config/routing.yml"
```

5) Update database:
----------------------------------
    php app/console doctrine:schema:update --force

6) add Bundle to assetic:
----------------------------------
```
assetic:
    debug:          %kernel.debug%
    use_controller: false
    bundles:        [ IbwSurveyBundle ]
```
7) Create a survey
----------------------------------
Go to `/survey/new` and complete all the required fields.
Save.
8) Include the Survey into your application
----------------------------------
Go into your base file and insert the following into the place you want the alert to be displayed:
```
{% include "IbwSurveyBundle:Default:include.html.twig" with {id: {survey_id} } %}
```
