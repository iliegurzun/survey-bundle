<?php

namespace Ibw\SurveyBundle\Service;
use FOS\UserBundle\Entity\User;

/**
 * Description of CheckEligibles
 *
 * @author Ilie Gurzun <ilie@intelligentbee.com>
 */
class CheckEligibles {
    
    
    private $container;
    
    public function __construct($serviceContainer)
    {
        $this->container = $serviceContainer;
    }
    
    public function checkEligibles($user, $survey)
    {
        $queryRes = $this->runQuery($survey);
        
        if(is_object($user))
        {
            foreach($queryRes as $key => $value)
            {
                if($value['user_id'] == $user->getId())
                {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    private function runQuery(\Ibw\SurveyBundle\Entity\Survey $survey)
    {
        $em = $this->container->get('doctrine')->getManager();
        
        $stmt = $em->getConnection()->prepare($survey->getCriteria());
        $stmt->execute();
        
        return $stmt->fetchAll();
    }
    
    public function checkUserSurvey($user, \Ibw\SurveyBundle\Entity\Survey $survey)
    {
        $em = $this->container->get('doctrine')->getManager();
        /* @var $surveyLogRepo \Ibw\SurveyBundle\Entity\SurveyLogRepository */
        $surveyLogRepo = $em->getRepository('IbwSurveyBundle:SurveyLog');
        $hasSurveys = $surveyLogRepo->checkIfUserCompletedSurvey($user, $survey);
        
        return count($hasSurveys) > 0;
    }
}
