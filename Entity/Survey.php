<?php

namespace Ibw\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ibw\SurveyBundle\Validator\Constraints\QueryConstraint;
use Ibw\SurveyBundle\Validator\Constraints\TemplateFileConstraint;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * Survey
 */
class Survey
{
    
    const TYPE_MODAL = 1;
    const TYPE_FLASH = 2;
    
    public static $surveyTypes = array(
        self::TYPE_MODAL        => 'Modal',
        self::TYPE_FLASH        => 'Flash',
    );
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('criteria', new QueryConstraint());
    }
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $displayType;

    /**
     * @var string
     */
    private $title;

    /**
     * @TemplateFileConstraint()
     * @var string
     */
    private $messageFile;

    /**
     * @var \DateTime
     */
    private $displayTime;

    /**
     * @var boolean
     */
    private $canBeDelayed;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var string
     */
    private $criteria;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set displayType
     *
     * @param integer $displayType
     * @return Survey
     */
    public function setDisplayType($displayType)
    {
        $this->displayType = $displayType;
    
        return $this;
    }

    /**
     * Get displayType
     *
     * @return integer 
     */
    public function getDisplayType()
    {
        return $this->displayType;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Survey
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set messageFile
     *
     * @param string $messageFile
     * @return Survey
     */
    public function setMessageFile($messageFile)
    {
        $this->messageFile = $messageFile;
    
        return $this;
    }

    /**
     * Get messageFile
     *
     * @return string 
     */
    public function getMessageFile()
    {
        return $this->messageFile;
    }

    /**
     * Set displayTime
     *
     * @param \DateTime $displayTime
     * @return Survey
     */
    public function setDisplayTime($displayTime)
    {
        $this->displayTime = $displayTime;
    
        return $this;
    }

    /**
     * Get displayTime
     *
     * @return \DateTime 
     */
    public function getDisplayTime()
    {
        return $this->displayTime;
    }

    /**
     * Set canBeDelayed
     *
     * @param boolean $canBeDelayed
     * @return Survey
     */
    public function setCanBeDelayed($canBeDelayed)
    {
        $this->canBeDelayed = $canBeDelayed;
    
        return $this;
    }

    /**
     * Get canBeDelayed
     *
     * @return boolean 
     */
    public function getCanBeDelayed()
    {
        return $this->canBeDelayed;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Survey
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set criteria
     *
     * @param string $criteria
     * @return Survey
     */
    public function setCriteria($criteria)
    {
        $this->criteria = $criteria;
    
        return $this;
    }

    /**
     * Get criteria
     *
     * @return string 
     */
    public function getCriteria()
    {
        return $this->criteria;
    }
}