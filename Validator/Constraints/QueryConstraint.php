<?php

namespace Ibw\SurveyBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class QueryConstraint extends Constraint
{
    public $message = 'The Criteria must not contain UPDATE or DELETE queries.';
}