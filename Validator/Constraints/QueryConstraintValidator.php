<?php

namespace Ibw\SurveyBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Description of QueryConstraint
 *
 * @author Ilie Gurzun <ilie@intelligentbee.com>
 */
class QueryConstraintValidator extends ConstraintValidator 
{
    public function validate($value, Constraint $constraint)
    {
        if (preg_match('/^(.*?(\bupdate\b)[^$]*)$/', $value, $matches) || 
                preg_match('/^(.*?(\bdelete\b)[^$]*)$/', $value, $matches)) {
            $this->context->addViolation(
                $constraint->message,
                array('%string%' => $value)
            );
        }
    }
}
