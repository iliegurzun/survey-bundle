<?php

namespace Ibw\SurveyBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Description of TemplateFileConstraint
 *
 * @author Ilie Gurzun <ilie@intelligentbee.com>
 */

/**
 * @Annotation
 */
class TemplateFileConstraint extends Constraint
{
    public $message = 'The File template does not exist.';
    
    public function validatedBy()
    {
        return 'TemplateFileConstraintValidator';
    }

}
