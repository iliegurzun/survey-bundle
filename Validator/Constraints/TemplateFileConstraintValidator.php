<?php

namespace Ibw\SurveyBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Description of TemplateFileConstraint
 *
 * @author Ilie Gurzun <ilie@intelligentbee.com>
 */

class TemplateFileConstraintValidator extends ConstraintValidator
{
    
    private $service_container;
   
    public function __construct($service_container)
    {
        $this->service_container = $service_container;
    }
    public function validate($value, Constraint $constraint)
    {
        try
        {
            $template = $this->service_container->get('templating')->render($value.'.html.twig');
        }
        catch(\Exception $ex)
        {
            $this->context->addViolation(
                $constraint->message,
                array('%string%' => $value)
            );
        }

    }
}
