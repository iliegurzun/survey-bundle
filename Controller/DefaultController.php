<?php

namespace Ibw\SurveyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    public function indexAction($surveyId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $surveyRepo = $em->getRepository('IbwSurveyBundle:Survey');
        $user = $this->getUser();
        if(!is_object($user))
        {
            return new JsonResponse(array(
                'success'       => true
            ), 200);
        }
        /* @var $survey \Ibw\SurveyBundle\Entity\Survey */
        $survey = $surveyRepo->find($surveyId);
        /* @var $eligiblesService \Ibw\SurveyBundle\Service\CheckEligibles */
        $eligiblesService = $this->container->get('ibw_survey.eligibles');
        if(!$survey)
        {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }
        $now = new \DateTime();
        if($survey->getDisplayTime() > $now)
        {
            return new JsonResponse(array(
                'success'       => true
            ), 200);
        }
        
        if(!$eligiblesService->checkEligibles($user, $survey) || !$survey->getIsActive())
        {
            
            return new JsonResponse(array(
                'success'       => true
            ), 200);
        }
        if($eligiblesService->checkUserSurvey($user, $survey))
        {
            return new JsonResponse(array(
                'success'       => true
            ), 200);
        }
        $template = null;
        try
        {
            switch($survey->getDisplayType())
            {
                case \Ibw\SurveyBundle\Entity\Survey::TYPE_MODAL:
                    $template = "IbwSurveyBundle:Default:modal.html.twig";
                    break;
                case \Ibw\SurveyBundle\Entity\Survey::TYPE_FLASH:
                    $template = "IbwSurveyBundle:Default:flash.html.twig";
                    break;
                default:
                    return new JsonResponse(array(
                        'success'   => false,
                        'error'      => 'This option does not exists!'
                    ), 500);
                    break;
            }
            
            return new JsonResponse(array(
                'success'       => true,
                'content'       => $this->renderView($template, array(
                    'data'          => $this->renderView($survey->getMessageFile().'.html.twig'),
                    'survey'        => $survey
                )),
                'templateType'  => $survey->getDisplayType(),
                'allowDismiss'  => $survey->getCanBeDelayed(),
            ), 200);
            
        } catch (\Exception $ex) {
            return new JsonResponse(array(
                'success'   => false,
                'error'      => $ex->getMessage()
            ), 500);
        }
    }
    
    public function logUserAccessedSurveyAction($surveyId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $surveyRepo = $em->getRepository('IbwSurveyBundle:Survey');
        $user = $this->getUser();
        if(!is_object($user))
        {
            return new JsonResponse(array(
                'success'       => true
            ), 200);
        }
        /* @var $survey \Ibw\SurveyBundle\Entity\Survey */
        $survey = $surveyRepo->find($surveyId);
        /* @var $eligiblesService \Ibw\SurveyBundle\Service\CheckEligibles */
        $eligiblesService = $this->container->get('ibw_survey.eligibles');
        if(!$survey)
        {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
        }
        $surveyLog = new \Ibw\SurveyBundle\Entity\SurveyLog();
        $surveyLog->setUserId($user->getId())
                ->setSurvey($survey);
        $em->persist($surveyLog);
        $em->flush();
        
        return new JsonResponse(array(
            'success'       => true
        ), 200);
    }
}
